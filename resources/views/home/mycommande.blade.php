@extends('layouts.admin')
@section('title','Gestion des categories')
@section('content')

   
   <h1> Commandes : </h1>

 <table id="tbl">
     <tr>
         <th>Id</th>
       <th>date_time</th>
       <th>Prix unitaire</th>
       <th>prix_total</th>
       <th>description</th>
       <th>Actions</th>
     </tr>
     @foreach ($mycommande as $id=> $item)
     
         <tr>
        
           <td>{{$id}}</td>
           <td>{{$item['date_time']}}</td>
           <td>{{$item['customer_id']}}</td>
           <td>{{$item['prix_total']}}MAD</td>
           <td> {{$item['description']}} </td>    
         </tr>
     @endforeach
     <tr>
       <th colspan="4">Total</th>
       <td colspan="2"> MAD</td>
    </tr>
   
@endsection