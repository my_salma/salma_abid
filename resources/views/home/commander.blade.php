@extends('layouts.admin')
@section('title','Gestion des categories')
@section('content')


<div id="form_commender" class="commander-form hidden">
  <form  action="{{ route('home.customerCommander') }}" method="POST">
      @csrf
      <div>
        <label for="nom">Nom</label>
        <input type="text" name="nom" id="nom" required>
      </div>
      
      <div>
        <label for="prenom">Prénom</label>
        <input type="text" name="prenom" id="prenom" required>
      </div>
      
      <div>
        <label for="telephone">Téléphone</label>
        <input type="text" name="telephone" id="telephone" required>
      </div>
      
      <div>
        <label for="ville">Ville</label>
        <input type="text" name="ville" id="ville" required>
      </div>
      
      <div>
        <label for="adresse">Adresse</label>
        <input type="text" name="adresse" id="adresse" required>
      </div>

      <div>
        <input id="valide" type="submit" value="Valider">
      </div>
  </form>
</div>

@endsection