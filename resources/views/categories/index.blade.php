 @extends('layouts.admin')
 @section('title','Gestion des categories')
 @section('content')
 <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    
  <h1>Liste des categories</h1>
    <a href="{{route('categories.create')}}">Ajouter une nouvelle categorie</a>
    <table id="tbl">
      <tr>
          <th>Id</th>
        <th>Designation</th>
        <th>Description</th>
        <th colspan="3">Actions</th>
      </tr>
      @foreach ($categories as $cat)
          <tr>
            <td>{{$cat->id}}</td>
            <td>{{$cat->designation}}</td>
            <td>{{$cat->description}}</td>
            <td><a href="{{route('categories.show',["category"=>$cat->id])}}">Details</a></td>
            <td><a href="{{route('categories.edit',["category"=>$cat->id])}}">Modifier</a></td>
            <td>
                <form action="{{route('categories.destroy',["category"=>$cat->id])}}" method="POST">
                    @method('DELETE')
                    @csrf
                    <input type="submit" value="Supprimer" onclick="return confirm('voulez-vous supprimer cette categorie?')">
                </form></td>
          </tr>
      @endforeach
    </table>
    <div class="d-flex justify-content-center">
      {{$categories->links('pagination::bootstrap-5')}}
    </div>
 @endsection