<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class commande extends Model
{
    use HasFactory;
    protected $tableName = "commande";
    protected $fillable= [
        'customer_id',
        'date_time',
        'description',
        'prix_total'
    ];
}
