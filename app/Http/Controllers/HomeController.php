<?php

namespace App\Http\Controllers;

use App\Models\Produit;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Commande;


class HomeController extends Controller
{
    public function index(){
        
        $produits=Produit::all([ 'id','designation','categorie_id','prix_u','quantite_stock',]);
        return view('home.index',compact('produits'));
    }


    public function add(Request $request,$id){
        $qte=$request->input('qte',1);
         $produit=Produit::find($id);
        $panier=$request->session()->get('panier',[]);
        if(isset($panier[$id])){
            $panier[$id]['qte']=$qte;
        }else{
        $panier[$id]=[
            'qte'=>$qte,
             'produit'=>$produit
        ];
    }
    
    $request->session()->put('panier',$panier);
    return redirect()->back();
        
    }


    public function show_panier(Request $request){
        $panier=$request->session()->get('panier',[]);
        $tot=0;
        foreach($panier as $id=>$item){
            $tot+=$item['qte']*$item['produit']->prix_u;
        }
        return view('home.panier',compact('panier','tot'));
    }


    public function delete(Request $request,$id){
        $panier=$request->session()->get('panier',[]);
        unset($panier[$id]);
        $request->session()->put('panier',$panier);
        return redirect()->back();
    }


    public function clear(Request $request ){

        $request->session()->forget('panier');
        // return redirect()->back();
        return redirect()->route('home.index');

    }

    public function commander(Request $request){
        $panier=$request->session()->get('panier',[]);
        $total=0;
        
        
        foreach($panier as $id=>$item){
            $total+=$item['qte']*$item['produit']->prix_u;
            
        }
        
        
        return view('home.commander',compact('total'));
    }

    public function customerCommander(Request $request)
{
    $customerdata = $request->validate([
        'nom'=>'required',
        'prenom'=>'required',
        'telephone'=>'required',
        'ville'=>'required',
        'adresse'=>'required',

    ]);

    $customer = Customer::create(['nom'=>$request->nom,'prenom'=>$request->prenom,'telephone'=>$request->telephone,'ville'=>$request->ville,'adresse'=>$request->adresse]);
    $customer_id = $customer->id;
    $customer_name = $customer->nom . ' ' . $customer->prenom;
    $date_time = now();
    $commanderdata=$request->validate([
        'description' => 'required',
        'prix_total' => 'required'
    ]);
    $commanderdata['customer_id'] = $customer_id;
    $commanderdata['date_time'] = $date_time;
    commande::create($commanderdata);
    $mycommandes=commande::all();
    return view('home.mycommande',compact('mycommandes'));

}
    
}